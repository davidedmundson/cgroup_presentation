/***************************************************************************
 *   Copyright (C) 2019 David Edmundson <davidedmundson@kde.org>                        *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA .        *
 ***************************************************************************/

import "presentation"
import QtQuick 2.6
import QtQuick.Layouts 1.3
//import QtMultimedia 5.12

Presentation
{
    id: presentation
    property bool square: true

    width: square ? 1024 : 1280
    height: square ? 768 : 720

    property variant fromSlide: null
    property variant toSlide: null

    property int transitionTime: 400
    
    fontFamily: "Noto Sans"
//     textColor: "#eeeeee"

    showNotes: false

    SlideCounter { id: slideCounter }

    Slide {
        centeredText: "Next-gen resource management"
    }

    Slide {
        title: "Authors"
        centeredText: "David Edmundson <kde@davidedmundson.co.uk>\nHenri Chain <henri.chain@enioka.com>"
    }

    Slide {
        centeredText: "The problem"
    }

    Slide {
        centeredText: "Managing running processes"
    }

    ImageSlide {
        title: "Yesteryear"
        source: "yesteryear.png"
    }

    ImageSlide {
        title: "Now"
        source: "now.png"
    }

    Slide {
        title: "Managing processes"
        content: ["Understanding what's going on is difficult",
                        "Aggregated resource stats are effectively meaningless",
        ]
    }

    //screenshot

    Slide {
        centeredText: "We need some metadata"
    }

    Slide {
        centeredText: "Fair resource distribution"
    }

    Slide {
        title: "Being fair"
        content: ["Discord is 13 processes",
                        "Krita is 1",
                        "A scheduler just sees opaque processes..."]
    }

    // could be a picture if I can be bothered

    Slide {
        centeredText: "We need some metadata"
    }

    Slide {
        centeredText: "Mapping processes to apps"
    }

    Slide {
        title: "Mapping processes to apps"
        content: ["The manager tries to map up windows to .desktop files",
                         "Hoping they report the right things",
                         "",
                         "We match up audio (by PID) to windows",
                         "With multi processes this is a guessing game"
        ]
    }

    Slide {
        centeredText: "We need some metadata"
    }

    // This should be ~10 minutes

    Slide {
        centeredText: "Solution"
    }

    // cgroups,
    // solved problem blah blah blah


    // There already is a solution!
    // You've probably heard of cgroups, a kernel feature introduced in 2007
    // It's essentially a partition of the process space that is hierarchical in nature
    // You can attach controllers on each group
    Slide {
        title: "Cgroups"
        content: [
            "Partitions the process space",
            "Hierarchical process groups that can have controllers assigned to them",
            "Battle tested on servers, container runtimes and flatpak",
        ]
    }

    // What are those resource controllers ?
    // - three main ones, affecting cpu usage, memory usage, and IO
    // - 3 basic functions, limiting, scheduling, accounting
    // - replace older functionality like nice, ulimit, ...
    // accounting is interesting battery
    // - IO is more difficult to setup right now. Made for servers with a specific configuration in mind
    Slide {
        title: "Resource control"
        content: [
            "cpu, mem, io controllers",
            "Limiting, scheduling, accounting, finer OOM control",
            "io controller: meant for servers, doesn't work out of the box yet"
        ]
    }

    // the way it influences scheduling is through weights
    // cpu weights Better than nice because it's:
    // - cgroup based rather than process based
    // - hierarchical, weights among siblings
    // - weights can push priority up or down (1 - 10000 range)
    // relative to siblings

    ImageSlide {
        title: "Weight-based scheduling: better than nice!"
        source: "treepie.png"
    }


    // So technically cgroups is managed through a simple filesystem interface in sysfs
    // But you will soon need a central daemon to manage them
    // The good news is that systemd has been supporting cgroups for a very long time
    // In fact, very systemd unit runs in its own cgroup
    // Systemd also delegates management of the user processes to its user instance
    // which manages cgroups owned by your user
    // Systemd also has a convenient dbus api that can be called from user space
    // And brings in configuration at a user and systemd level with so called drop in units
    // Which means you can predefine behavior for certain apps
    Slide {
        title: "Managing Cgroups"
        content: [
            "A job for the service manager",
            "Every systemd unit already runs in its own cgroup",
            "Systemd's user instance is the owner of the user cgroup tree",
            "DBus API",
            "Drop ins: user and distribution -level configuration"
        ]
    }

    // This is what a typical systemd cgroup tree looks like
    Slide {
        title: "Systemd cgroup tree"
        Text {
            font.pixelSize: 18
            font.family: presentation.codeFontFamily
            text: "$ systemd-cgls\n-.slice\n├─user.slice \n│ └─user-1000.slice \n│   ├─user@1000.service \n│   │ ├─app.slice \n│   │ │ ├─app-org.kde.konsole-86ba3864a585460eba07df01d8d0a514.scope \n│   │ │ │ ├─33146 /usr/bin/konsole\n│   │ │ │ └─33158 /bin/bash\n│   │ │ └─app-firefox-bf4861eb03f54a5f9c92c5da35698f96.scope \n│   │ │   ├─2165 /usr/lib/firefox/firefox\n│   │ │   └─5215 /usr/lib/firefox/firefox -contentproc -childID 11 -isForBrowse…\n│   │ ├─background.slice \n│   │ │ └─app-org.kde.krunner.service \n│   │ ├─ssh-agent.service \n│   │ │ └─1112 /usr/bin/ssh-agent -D -a /run/user/1000/ssh-agent.socket\n│   │ └─dbus.service \n│   │   └─20233 /usr/bin/ksysguardd\n│   └─session-2.scope \n│     ├─ 1103 /usr/lib/sddm/sddm-helper --socket /tmp/sddm-authf5d4f987-45a0-45…\n│     ├─ 1122 /usr/bin/startplasma-x11\n"
        }
    }

    // Cgroups have been redesigned in 2015, solving many architectural issues with the previous API
    // Currently all distributions expose the new api (hybrid hierarchy), but only fedora uses full cgroups v2 by default
    // In practice this means systemd is always capable of doing the grouping, but some controllers are not available to the user instance
    Slide {
        title: "cgroups v2: status of support"
        content: [
            "Processes can belong to only one cgroup",
            "Multiple controllers on a cgroup",
            "Unified hierarchy: the future, not supported by docker though",
            "Grouping works in all cases",
            "Attaching controllers with user systemd not supported for cgroups v1"
        ]
    }

    // the new api also introduces slices which are the inner nodes of the tree
    // Once they have a child cgroup, they can't contain processes, only other cgroups
    Slide {
        title: "Slices"
        content: [
            "v2 inner leaves cannot contain processes",
            "Called slices in systemd",
            "Can also have drop ins"
        ]
        Image {
            source: "./slices.jpg"
            height: parent.height / 2
            anchors.bottom: parent.bottom
            anchors.left: parent.left
            anchors.right: parent.right
            fillMode: Image.PreserveAspectFit
            z: -100
        }
    }

    // So how can we leverage systemd to manage our cgroups when launching an app ?
    Slide {
        title: "Scopes vs Services"
        content: ["systemd can also handle transient units",
            "Scopes - we launch a process, then tag it in a cgroup",
            "Services - the cgroup controller launches the process"]
    }

    // What we have so far
    Slide {
        title: "Current rollout"
        content: ["Plasma 5.19 has every app spawned in it's own cgroup",
                         "Lots of effort needed to put all KDE code through the same API to launch processes",
                         "There are dozens of edge cases to identify and clean up"
                 ]
    }

    Slide {
        title: "Current rollout"
        content: ["Goal is to do everything safely",
                         "No regressions!",
                         "First we deploy us using this",
                         "Then start using this metadata"
                 ]
    }

    Slide {
        title: "Work using this"
        content: ["Linux task scheduler is now using this already",
                        "Taskmanager patches are on review",
                        "libksysguard can surface this information"
        ]
    }

    ImageSlide {
        source: "ksysguard_old.png"
    }

    ImageSlide {
        source: "ksysguard_new.png"
    }

    ImageSlide {
        title: "Making a standard"
        source: "standards.png"
    }

    // The future

    Slide {
        centeredText: "Adding more features on top"
    }

    Slide {
        centeredText: "<b>Un</b>fair resource destribution"
    }

    Slide {
        centeredText: "Statically assigned resources"
    }

    Slide {
        title: "Default slices"
        content: ["Session: kwin, plasmashell, etc", "Apps: firefox, konversation, etc.", "Background: kded, xembedsniproxy, etc.", "", "Each slice will have a natural \"boost\" applied"]
        //emphasis on OOM too
    }

    Slide {
        title: "Extra configurability"
        content: ["Limits can take us further", "An app, distro, or user can add \"drop in\" to configure individual limits of a background services", "eg limit a file indexer to only 10% CPU ever", "Set OOM stats", "Limit forkbombs"]
    }

    // We can also change resource allocation based on runtime information
    // for instance, we can prioritize the focused application
    Slide {
        title: "Favouring interactive applications"
        content: [
            "We can boost the active application relative to other applications"
        ]
    }

    // Working towards that goal, I started a small library
    // That wraps the systemd dbus api
    // to provide resource control for all running apps from any KDE application
    // You're welcome to go have a look at the code and start hacking on it
    Slide {
        title: "KCGroups"
        content: [
            "Wraps the systemd dbus api",
            "Exposes resource control",
            "Fully async, can be used from QML",
            "Foreground booster demo app",
            "https://invent.kde.org/libraries/kcgroups"
        ]
    }

    // It includes a example app that talks to your window manager
    // to determine the currently focused app
    // and gives it a high CPU weight
    // You can watch a demo of it in the link at the end of the presentation
    ImageSlide {
        title: "Foreground app booster"
        source: "booster.png"
    }

    // One of the things we are working on is moving to systemd services
    // That are launched by systemd
    // We get much improved logging, ahead of time configuration
    // AKA we can have a file that says "all instances of baloo should only get 20% cpu"
    // It also fixes application grouping for systemd before version 238
    // We have an implementation ready, hidden behind a flag,
    // But we have run into some systemd limitations
    Slide {
        title: "Move to systemd services"
        content: ["fully managed by systemd",
            "Improve logging",
            "Some drop-in configuration only works with services",
            "User scopes buggy for old systemd", // means no grouping with plasma 1.19 for systemd<238
        ]
    }

    Slide {
        title: "Move to systemd services"
        content: [
            "New implementation mostly ready, guarded behind flag",
            "Due to current systemd limitations for non-daemon services",
            "More work needed to get crash handling to work"
        ]
    }

    Slide {
        title: "Startup"
        content: ["Have entire plasma session startup be managed by systemd",
            "Brings all the resource management to the session too",
            "Logs for key session processes in the right place",
            "Autostart files work the same",
            "Familar experience for sysadmins"
        ]
    }

    Slide {
        title: "Further reading"
        content: ["Upstream specification - https://systemd.io/DESKTOP_ENVIRONMENTS/",
                         "Code examples - https://invent.kde.org/snippets/1111",
                         "Guadec - https://www.youtube.com/watch?v=cmYCM3S_YEY ",
                         "Foreground booster demo - https://youtu.be/o6_uq5AQOHg"
                 ]
    }

    Slide {
        centeredText: "Q&A"
    }

}
