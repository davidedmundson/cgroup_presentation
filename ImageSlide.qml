import "presentation"
import QtQuick 2.0

Slide
{
//     anchors.fill: parent
    property alias source: image.source
    Image {
        id: image
        anchors.fill: parent
        fillMode: Image.PreserveAspectFit
        z: -100
    }
}
